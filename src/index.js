import { createStore } from 'redux'
import { Provider } from 'react-redux'
import todoApp from './reducers'

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/App';
import registerServiceWorker from './registerServiceWorker';

//store仅有一个，在react-dom的render方法中实例化即可。
var store = createStore(todoApp);

var rootElement = document.getElementById('root');

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  rootElement
);

registerServiceWorker();
